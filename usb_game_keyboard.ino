// Implements a simple USB keyboad from switches. The switches
// needs to be normally open and connected between ground and
// one digital pin. The digital pins are configured as pull-up.
// Debouncing time is configured in step of 10ms

#include <Keyboard.h>

// Button pin and corresponding keyboard keys configuration
#define NKEYS 6
int pins[NKEYS] = {2, 3, 4, 5, 6, 7};
int keys[NKEYS] = {KEY_DOWN_ARROW,
                   KEY_RIGHT_ARROW,
                   KEY_UP_ARROW,
                   KEY_LEFT_ARROW,
                   KEY_ESC,
                   ' '};

// Debounce time is 30ms
#define DEBOUNCE 3

//end of configuration ///////////////////////////
int states[NKEYS];

void setup() {
  Keyboard.begin();
  
  for (int i=0; i<NKEYS; i++) {
    pinMode(pins[i], INPUT_PULLUP);
  }
  // Debounce pins
  delay(100);
}

void loop() {
  for(int i=0; i<NKEYS; i++) {
    int state = digitalRead(pins[i]);

    if (state == LOW) {
      // Press key if it has been seen pressed for at least
      // "DEBOUNCE" time frame
      if (states[i] >= DEBOUNCE) {
         Keyboard.press(keys[i]);
      } else {
        states[i]++;
      }
    } else {
      // Release key if it has been pressed
      if(states[i] >= DEBOUNCE) {
        Keyboard.release(keys[i]);
      }
      states[i] = 0;
    }
  }

  delay(10);
}
